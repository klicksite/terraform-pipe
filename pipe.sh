#!/usr/bin/env sh

if [ ! -z "$TERRAFORM_SOURCE_DIR" ]
then
 [ ! -z "$DEBUG" ] && echo "TERRAFORM SOURCE: ${TERRAFORM_SOURCE_DIR}"
 cd $TERRAFORM_SOURCE_DIR
fi
 terraform init
if [ ! -z "$TERRAFORM_WORKSPACE" ]
then
 terraform workspace select $TERRAFORM_WORKSPACE
fi

if [ -z "$TERRAFORM_COMMANDS" ]
then
  export TERRAFORM_COMMANDS=validate,plan
fi

for command in $(echo $TERRAFORM_COMMANDS | tr "," "\n")
do
  case $command in
  validate) terraform validate ;;
  plan) terraform plan ;;
  apply) terraform apply  -auto-approve ;;
  *) echo "Opcao Invalida!" ;;
  esac
done