FROM hashicorp/terraform:light

COPY pipe.sh /

RUN chmod +x /pipe.sh

ENTRYPOINT ["/pipe.sh"]
